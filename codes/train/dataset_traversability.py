from torch.utils.data import Dataset
import numpy as np
from PIL import Image
import os
import glob
from scipy import ndimage

class Dataset(Dataset):
    def __init__(self, data_file, label_file, mask_file):
        # super(Dataset).__init__()
        self.data = np.load(data_file, mmap_mode='r')
        self.labels = np.load(label_file, mmap_mode='r')
        self.visible_mask = np.load(mask_file, mmap_mode='r')
        # self.data = np.load(data_file)
        # self.labels = np.load(label_file)
        # self.visible_mask = np.load(mask_file)

    def __getitem__(self, idx):
        # label_file = '{:06}'.format(idx) + '_label.npz'
        # data = np.load(self.root_dir+label_file)
        #
        # input = data['input']
        # label = data['label']
        # vis = data['visible_mask']
        #

        input = np.asarray(self.data[idx]).astype('f4').transpose((2, 0, 1))
        labels = np.asarray(self.labels[idx]).astype('i8')
        new_labels = labels.copy()
        new_labels[labels == 1] = 0
        new_labels[labels == 2] = 1
        new_labels[labels == 0] = 2
        vis = np.asarray(self.visible_mask[idx]).astype(np.float32)

        vis[~np.isnan(vis)] = 0
        vis[np.isnan(vis)] = 1
        bw_dist = ndimage.distance_transform_edt(vis)
        vis = (bw_dist < 5) # distance in decimeters from visible
        mask = ~np.isnan(vis).reshape((256, 256))
        mask = mask.astype(np.float32)
        vis = vis.astype(np.float32)
        input[np.isnan(input)] = 0

        sample = {'input': input, 'label': new_labels, 'mask': mask, 'weights': vis}
        #if self.transform:
        #    sample = self.transform(sample)
        return sample

    def __len__(self):
        return self.data.shape[0]
