import torch
import net_traversability
import dataset_traversability
from time import gmtime, strftime
# from tensorboardX import SummaryWriter
import os
import torch.optim as optim
import numpy as np
import torch.nn as nn
import torch.nn.functional as F


def loss_function(weight, gamma=2):
    def focal_loss(prediction, labels):
        tweight = torch.from_numpy(weight).to(prediction.device).float()
        prediction_prob = F.softmax(prediction, dim=1)

        # cross entropy part
        result = F.cross_entropy(prediction, labels, reduction='none', weight=tweight)[:, None, ...]

        # new part
        loss_weight = (1 - prediction_prob.gather(1, labels[:, None, ...])) ** gamma

        # final form
        loss = loss_weight * result

        return torch.mean(loss)
    return focal_loss


if __name__ == '__main__':
    epochs = 1000
    batch_size = 24
    learning_rate = np.ones([epochs, 1])*0.1


    runtime = strftime("%Y-%m-%d_%H:%M:%S", gmtime())
    output_file = "../bc_project/data/traversability_net/run_"+runtime
    # writer = SummaryWriter('../bc_project/data/traversability_net/tensorboardX/run_' + runtime)

    if not os.path.exists(output_file):
        os.makedirs(output_file)

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    net = net_traversability.Net()
    net = net.to(device)
    net.train()

    dataset_trn = dataset_traversability.Dataset("/home.nfs/docekja4/traversability_estimation/indoor_dataset2/data.npy", "/home.nfs/docekja4/traversability_estimation/indoor_dataset2/labels.npy", "/home.nfs/docekja4/traversability_estimation/indoor_dataset2/visible_mask.npy")
    trainloader = torch.utils.data.DataLoader(dataset_trn, batch_size=batch_size, shuffle=True, num_workers=2)

    dataset_val = dataset_traversability.Dataset("/home.nfs/docekja4/traversability_estimation/indoor_dataset2/data_val.npy", "/home.nfs/docekja4/traversability_estimation/indoor_dataset2/labels_val.npy", "/home.nfs/docekja4/traversability_estimation/indoor_dataset2/visible_mask_val.npy")
    valloader = torch.utils.data.DataLoader(dataset_val, batch_size=batch_size, shuffle=True, num_workers=2)
    #TODO copyfile('train.py', 'net-' + runtime + '/train_script.py')

    seen = 0

    lossfun = loss_function(np.array([1, 1, 0]))

    for epoch in range(epochs):  # loop over the dataset multiple times
        # optimizer = optim.SGD(net.parameters(), lr=0.00001, momentum=0.9, weight_decay=0.1)
        optimizer = optim.Adam(net.parameters(), lr=0.001)
        epoch_loss = 0
        for i, data in enumerate(trainloader):
            input = data['input']
            label = data['label']
            input_mask = data['mask']
            weights = data['weights']

            input, label, input_mask, weights = input.to(device), label.to(device), input_mask.to(device), weights.to(device)

            output = net(input, input_mask)
            # loss = net_traversability.weighted_mse_loss(output, label, weights)
            loss = lossfun(output, label.squeeze(3))
            loss.backward()

            epoch_loss += loss

            optimizer.step()
            optimizer.zero_grad()
            # writer.add_scalar('data/loss', loss, seen)
            """
            if i%100==0:
                input = input[0, 0, :, :].reshape((1, 256, 256))
                input = input.cpu().numpy()
                # input = input.transpose((1, 2, 0))
                input = input - input.min()
                input = input / input.max()
                writer.add_image('data/Image', input, seen)
                label = label[0, :, :, :]
                label = np.transpose(label, (2, 0, 1))
                label = label.cpu().numpy()
                label = label - label.min()
                label = label / label.max()
                writer.add_image('data/Label', label, seen)
                # out = torch.sigmoid(output[0,:, :, :].clone())
                out = output[0, :, :, :].detach().cpu().numpy()
                out = out - out.min()
                out = out / out.max()
                writer.add_image('data/Output', out, seen)
            seen += 1

        writer.add_scalar('data/epoch_loss', epoch_loss, seen) """
        print("Epoch train loss: " + str(epoch_loss))
        epoch_loss = 0
        epoch_val_loss = 0

        for i, data in enumerate(valloader):
            input = data['input']
            label = data['label']
            input_mask = data['mask']
            weights = data['weights']

            input, label, input_mask, weights = input.to(device), label.to(device), input_mask.to(device), weights.to(
                device)
            with torch.no_grad():
                output = net(input, input_mask)
                # loss = net_traversability.weighted_mse_loss(output, label, weights)
                loss = lossfun(output, label.squeeze(3))
            epoch_val_loss += loss
            """
            if i%100==0:
                input = input[0, 0, :, :].reshape((1, 256, 256))
                input = input.cpu().numpy()
                # input = input.transpose((1, 2, 0))
                input = input - input.min()
                input = input / input.max()
                writer.add_image('data/val_Image', input, seen)
                label = label[0, :, :, :]
                label = np.transpose(label, (2, 0, 1))
                label = label.cpu().numpy()
                label = label - label.min()
                label = label / label.max()
                writer.add_image('data/val_Label', label, seen)
                # out = torch.sigmoid(output[0,:, :, :].clone())
                out = output[0, :, :, :].detach().cpu().numpy()
                out = out - out.min()
                out = out / out.max()
                writer.add_image('data/val_Output', out, seen)
        writer.add_scalar('data/val_loss', epoch_loss, seen) """
        print("Epcho validation loss: " + str(epoch_val_loss))
        torch.save(net.state_dict(), output_file+'/net_epoch_{:04}'.format(epoch))


        # for i, data in enumerate(valloader)
