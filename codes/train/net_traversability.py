import torch.nn as nn
import torch
import numpy as np
import random

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.relu = nn.LeakyReLU(0.1, inplace=True)
        self.pool = nn.MaxPool2d(2, 2)
        self.sp_conv1 = Sparse_Conv2d(1, 16, 5, 1, 2)
        self.sp_conv2 = Sparse_Conv2d(16, 32, 5, 1, 2)
        self.sp_conv3 = Sparse_Conv2d(32, 64, 5, 1, 2)
        self.sp_conv4 = Sparse_Conv2d(64, 128, 5, 1, 2)

        self.conv1_en0 = nn.Conv2d(17, 16, 5, 1, 2)
        self.conv2_en0 = nn.Conv2d(16, 32, 5, 1, 2)
        self.conv3_en0 = nn.Conv2d(32, 64, 5, 1, 2)

        self.conv1_en1 = nn.Conv2d(16, 16, 5, 1, 2, bias=True)
        self.conv1_en2 = nn.Conv2d(16, 16, 5, 1, 2, bias=True)
        self.conv2_en1 = nn.Conv2d(32, 32, 5, 1, 2, bias=True)
        self.conv2_en2 = nn.Conv2d(32, 32, 5, 1, 2, bias=True)
        self.conv2_en3 = nn.Conv2d(32, 32, 5, 1, 2, bias=True)

        self.conv3_en1 = nn.Conv2d(64, 64, 5, 1, 2, bias=True)
        self.conv3_en2 = nn.Conv2d(64, 64, 5, 1, 2, bias=True)
        self.conv3_en3 = nn.Conv2d(64, 64, 5, 1, 2, bias=True)


        self.conv4 = nn.Conv2d(16, 1, 5, 1, 2, bias=False)
        self.conv3 = nn.Conv2d(16, 1, 5, 1, 2, bias=False)

        self.conv2 = nn.Conv2d(16, 3, 3, 1, 1,bias=False)
        self.conv1 = nn.Conv2d(32, 16, 3, 1, 1,bias=False)

        self.deconv1 = nn.ConvTranspose2d(1, 1, kernel_size=2, stride=2, padding=0, output_padding= 0)

        self.deconv2 = nn.ConvTranspose2d(64, 64, kernel_size=2, stride=2, padding=0, output_padding= 0)
        self.deconv3 = nn.ConvTranspose2d(32, 32, kernel_size=2, stride=2, padding=0, output_padding= 0)
        self.deconv4 = nn.ConvTranspose2d(16, 16, kernel_size=2, stride=2, padding=0, output_padding= 0)

        self.upsample1 = nn.Upsample(scale_factor=2, mode='bilinear')
        self.upsample2 = nn.Upsample(scale_factor=2, mode='bilinear')
        self.upsample3 = nn.Upsample(scale_factor=2, mode='bilinear')



    def forward(self, input, mask):
        x = self.pool(self.relu(self.conv1_en0(input)))
        x = self.relu(self.conv1_en1(x))
        x1 = self.relu(self.conv1_en2(x))
        m = self.pool(mask)
        x = self.pool(self.relu(self.conv2_en0(x1)))
        x = self.relu(self.conv2_en1(x))
        x = self.relu(self.conv2_en2(x))
        x2 = self.relu(self.conv2_en3(x))
        #x = self.pool(self.relu(self.conv3_en0(x)))
        #x = self.relu(self.conv3_en1(x))
        #x = self.relu(self.conv3_en2(x))
        #x = self.relu(self.conv3_en3(x))
        #m = self.pool(m)
        #x = self.pool(self.relu(self.sp_conv3(x, m)))
        #x = self.relu(self.conv3_en(x))
        #m = self.pool(m)

        x = self.upsample1(x2)
        x = self.relu(self.conv1(x))

        x = self.upsample2(x)
        x = (self.conv2(x))

        #x = self.upsample3(x)
        #x = (self.conv3(x))

        #x = self.relu(self.deconv1(x))
        #x = self.upsample1(x)
       # x = self.relu(self.conv1(x))
       # x = self.upsample2(x)

         #  x = self.conv2(x)

        '''
        x = self.deconv1(x)
        x = self.relu(self.conv1(x))
        x = self.deconv2(x)
        x = self.relu(self.conv2(x))
        x = (self.deconv3(x))
        #x = self.relu(self.deconv3(x))
        x = self.relu(self.conv3(x))
        x = (self.deconv4(x))
        x = self.conv4(x)
        '''
        return x


class Sparse_Conv2d(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, stride, padding):
        super(Sparse_Conv2d, self).__init__()
        self.in_ch = in_ch
        self.out_ch = out_ch
        self.kernel_size = kernel_size
        self.padding = padding
        self.stride = stride
        self.conv_data = nn.Conv2d(self.in_ch, self.out_ch, self.kernel_size, self.stride, self.padding, bias=False)
        self.conv_mask = nn.Conv2d(1, self.out_ch, self.kernel_size, self.stride, self.padding, bias=False)
        self.conv_mask.weight.data = torch.ones(self.conv_mask.weight.data.shape)
        self.conv_mask.weight.requires_grad = False
        self.kernel_num_elem = kernel_size*kernel_size

    def forward(self, input, mask):
        x = self.conv_data(input)
        m = self.conv_mask(mask)
        sp = torch.div(m, self.kernel_num_elem)
        output = torch.mul(x, sp)
        return x


def weighted_mse_loss(output, target, weight):
    loss = torch.sqrt(torch.mean(weight * (output - target) ** 2)*weight.sum())
    return loss
