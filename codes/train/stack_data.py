import sys
import os
import os.path
import numpy as np

SHAPE = 50
NAN_DEF = -500

def stack_data(directory):
    data = None
    labels = None
    visible = None
    (dirpath, directories, files) = next(os.walk(directory))
    cnt = 1
    for dir in directories:
        path = os.path.join(directory, dir)
        filelist = [os.path.join(path, f) for f in os.listdir(path)]

        for data_array in filelist:
            if data_array.endswith(".npy") and data_array.find("bag_features_") != -1:
                labels_array = get_index(filelist, data_array)
                visible_mask = get_index_vis(filelist, data_array)
                # missing labels
                if labels_array is None or visible_mask is None:
                    continue

                # stack data, labels
                if data is None:
                    data = np.reshape(np.load(data_array), (1, 17, 256, 256))
                    labels = np.reshape(np.load(labels_array), (1, 256, 256))
                    visible = np.reshape(np.load(visible_mask), (1, 256, 256))
                else:
                    next_data = np.reshape(np.load(data_array), (1, 17, 256, 256))
                    data = np.concatenate((data, next_data), axis=0)
                    next_labels = np.reshape(np.load(labels_array), (1, 256, 256))
                    labels = np.concatenate((labels, next_labels), axis=0)
                    next_visible = np.reshape(np.load(visible_mask), (1, 256, 256))
                    visible = np.concatenate((visible, next_visible), axis=0)
        print("Evaluated " + str(cnt) + "/" + str(len(directories)))
        cnt += 1

    data = np.transpose(data, (0, 2, 3, 1))
    labels = np.transpose(labels[np.newaxis], (1, 2, 3, 0))
    visible = np.transpose(visible[np.newaxis], (1, 2, 3, 0))
    data_trn, labels_trn, visible_trn, data_val, labels_val, visible_val = separate_data(data, labels, visible)

    np.save(directory + "data.npy", data_trn)
    np.save(directory + "labels.npy", labels_trn)
    np.save(directory + "visible_mask.npy", visible_trn)
    np.save(directory + "data_val.npy", data_val)
    np.save(directory + "labels_val.npy", labels_val)
    np.save(directory + "visible_mask_val.npy", visible_val)
    print("Size of dataset is: " + str(data_trn.shape[0]))
    return


def stack_data_2(directory):
    data = None
    labels = None
    (dirpath, directories, files) = next(os.walk(directory))
    for dir in directories:
        path = os.path.join(directory, dir)
        filelist = [os.path.join(path, f) for f in os.listdir(path)]

        for data_array in filelist:
            if data_array.endswith(".npy") and data_array.find("bag_data") != -1:
                labels_array = get_index(filelist, data_array)

                # missing labels
                if labels_array is None:
                    continue

                # stack data, labels
                if data is None:
                    data = np.reshape(normalization(np.load(data_array), NAN_DEF), (1, SHAPE, SHAPE))
                    labels = np.reshape(np.load(labels_array), (1, SHAPE, SHAPE))
                else:
                    next_data = np.reshape(normalization(np.load(data_array), NAN_DEF), (1, SHAPE, SHAPE))
                    data = np.concatenate((data, next_data), axis=0)
                    next_labels = np.reshape(np.load(labels_array), (1, SHAPE, SHAPE))
                    labels = np.concatenate((labels, next_labels), axis=0)

    data = np.reshape(data, (data.shape[0], SHAPE, SHAPE, 1))
    # labels = np.reshape(labels, (labels.shape[0], SHAPE, SHAPE, 1))

    data_trn, labels_trn, data_val, labels_val = separate_data(data, labels)

    np.save(directory + "data.npy", data_trn)
    np.save(directory + "labels.npy", labels_trn)
    np.save(directory + "data_val.npy", data_val)
    np.save(directory + "labels_val.npy", labels_val)
    return


def separate_data(data, labels, visible):
    random_ind = np.random.rand(data.shape[0])
    data_trn = data[random_ind >= 0.1]
    labels_trn = labels[random_ind >= 0.1]
    visible_trn = visible[random_ind >= 0.1]
    data_val = data[random_ind < 0.1]
    labels_val = labels[random_ind < 0.1]
    visible_val = visible[random_ind < 0.1]


    return data_trn, labels_trn, visible_trn, data_val, labels_val, visible_val


def normalization(array, nan_def):
    # new_arr = array[array != nan_def]
    new_arr = array[np.logical_not(np.isnan(array))]
    arr_min = min(new_arr)
    # array[array != nan_def] = array[array != nan_def] - arr_min
    array[np.logical_not(np.isnan(array))] = array[np.logical_not(np.isnan(array))] - arr_min
    # array[array == nan_def] = 0
    array[np.isnan(array)] = 0

    return array

def get_index_vis(array, file):
    ind_of_start = file.find("bag_features_") + len("bag_features_")

    index = 0

    while True:
        try:
            index = index*10 + int(file[ind_of_start])
            ind_of_start += 1
        except:
            break

    ind_of_start = file.find("bag_features_")

    ret_file = file[:ind_of_start] + "bag_visible_mask_" + str(index) + ".npy"
    if ret_file in array:
        return ret_file
    else:
        return None


def get_index(array, file):
    ind_of_start = file.find("bag_features_") + len("bag_features_")

    index = 0

    while True:
        try:
            index = index*10 + int(file[ind_of_start])
            ind_of_start += 1
        except:
            break

    ind_of_start = file.find("bag_features_")

    ret_file = file[:ind_of_start] + "bag_labels_" + str(index) + ".npy"
    if ret_file in array:
        return ret_file
    else:
        return None


if __name__ == "__main__":
    stack_data(sys.argv[1])
