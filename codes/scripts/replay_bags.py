import sys
import os
import os.path


def make_dataset(dir_path, dataset_path):
    # filelist = [os.path.join(dir_path + "bagfiles_big/", f) for f in os.listdir(dir_path + "bagfiles_big/")]

    g = open("/home/honza/info_dataset.txt", 'w+')
    # for bagpath in filelist:
    #     if bagpath.endswith(".bag"):
    #         bagname = os.path.basename(bagpath)
    #         # MAKE DATA AND LABELS
    #         command = "roslaunch /home/honza/bc_project/record_topics.launch bag:=" + bagpath + " name:=" + dataset_path + bagname[:-4] + "_replay.bag"
    #         os.system(command)
    #
    #         g.write(bagname + "\n")
    # filelist = [os.path.join(dir_path + "bagfiles/", f) for f in os.listdir(dir_path + "bagfiles/")]
    #
    # for bagpath in filelist:
    #     if bagpath.endswith(".bag"):
    #         bagname = os.path.basename(bagpath)
    #         # MAKE DATA AND LABELS
    #         command = "roslaunch /home/honza/bc_project/record_topics.launch bag:=" + bagpath + " name:=" + dataset_path + bagname[:-4] + "_replay.bag"
    #         os.system(command)
    #
    #         g.write(bagname + "\n")
    filelist = [os.path.join(dir_path + "bagfiles_add2/", f) for f in os.listdir(dir_path + "bagfiles_add2/")]
    for bagpath in filelist:
        if bagpath.endswith(".bag"):
            bagname = os.path.basename(bagpath)
            # MAKE DATA AND LABELS
            command = "roslaunch /home/honza/bc_project/record_topics.launch bag:=" + bagpath + " name:=" + dataset_path + bagname[:-4] + "_replay.bag"
            os.system(command)

            g.write(bagname + "\n")
    return


if __name__ == "__main__":
    make_dataset(sys.argv[1], sys.argv[2])
