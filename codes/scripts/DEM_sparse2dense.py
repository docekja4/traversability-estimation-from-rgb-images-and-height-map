#!/usr/bin/env python
import numpy as np
import rospy
import matplotlib.pyplot as plt
from ros_numpy import numpify
from voxel_map import VoxelMap
from sensor_msgs.msg import PointCloud2
import sensor_msgs.point_cloud2 as pc2
from nav_msgs.msg import OccupancyGrid
from OccupancyMap import OccupancyMap, filter_pc
import tf2_ros

DEBUG = True

class DEM_sparse2dense():
    def __init__(self):
        rospy.init_node('DEM_sparse2dense')
        self.grid_res = 0.1
        self.map_frame = 'map'

        self.tf = tf2_ros.Buffer(cache_time=rospy.Duration(30))
        self.tf_sub = tf2_ros.TransformListener(self.tf)

        self.input_map3d = OccupancyMap(self.map_frame, self.grid_res)
        self.input_map2d = OccupancyMap(self.map_frame, self.grid_res)

        self.points_sub = rospy.Subscriber(rospy.get_param("~bigbox_topic", '/dynamic_point_cloud_bigbox'), PointCloud2, self.points_cb, queue_size=2)

        self.input_pc_pub = rospy.Publisher('input_pc', PointCloud2, queue_size=2)
        self.input_filtered_pc_pub = rospy.Publisher('input_filtered_pc', PointCloud2, queue_size=2)
        self.input_2d_pc_pub = rospy.Publisher('input_2d_pc',  PointCloud2, queue_size=2)
        self.output_2d_pc_pub = rospy.Publisher('output_2d_pc',  PointCloud2, queue_size=2)
        self.output_3d_pc_pub = rospy.Publisher('output_3d_pc',  PointCloud2, queue_size=2)


    def points_cb(self, msg):
        if (rospy.get_rostime() - msg.header.stamp).secs > 5:
            print('old msg')
            print((rospy.get_rostime() - msg.header.stamp).secs)
            return
        # get TF
        sensor_to_map = self.tf.lookup_transform(self.map_frame, msg.header.frame_id, msg.header.stamp)

        # TODO: FIFO with only surrounding points in robot surroundings
        # create pc from msg
        pc_cloud = numpify(msg)
        pc_array_homo = np.concatenate([np.asarray([pc_cloud['x'].ravel(),pc_cloud['y'].ravel(),pc_cloud['z'].ravel()]), np.ones_like(pc_cloud['z'])[np.newaxis]])
        T = numpify(sensor_to_map.transform)
        # cloud in map frame
        pc_cloud_in_map = np.matmul(T, pc_array_homo)

        # creates sensor origin in map frame
        origins = np.ones_like(pc_cloud['x']) * np.array(
            [sensor_to_map.transform.translation.x, sensor_to_map.transform.translation.y,
             sensor_to_map.transform.translation.z], )[np.newaxis].T

        # create 3d voxel map
        self.input_map3d.voxel_map.update_lines(origins, pc_cloud_in_map[:3, :])

        if DEBUG:
            self.input_pc_pub.publish(self.input_map3d.to_pc_msg(self.map_frame))

        # filter ceilings and measurements outliers
        self.input_map3d = filter_pc(self.input_map3d)

        if DEBUG:
            self.input_filtered_pc_pub.publish(self.input_map3d.to_pc_msg(self.map_frame))

        # 2d from 3d
        self.input_map2d = self.input_map3d.to_2d(self.input_map2d, sensor_to_map)

        if DEBUG:
            self.input_2d_pc_pub.publish(self.input_map2d.to_pc_msg(self.map_frame, -np.inf))

        # grid in robot frame
        robot_grid = np.meshgrid(np.arange(-12.75, 12.85, 0.1), np.arange(-12.75, 12.85, 0.1))
        robot_grid_array = np.asarray([robot_grid[0].ravel(), robot_grid[1].ravel(), np.ones_like(robot_grid[1].ravel()) * self.grid_res])
        # to homogeneous coordinates
        robot_grid_array = np.concatenate([robot_grid_array, np.ones(robot_grid_array.shape[1])[np.newaxis]]) # not sure about z=0.1
        # transform robot grid to map frame
        robot_grid_mapframe = np.matmul(T, robot_grid_array)
        # fill the input from 2d map
        robot_grid_mapframe_2d = np.concatenate([robot_grid_mapframe[0:2, :], np.ones(robot_grid_array.shape[1])[np.newaxis] * self.grid_res / 2]) # x,y,z in 2.5d map
        v = self.input_map2d.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(robot_grid_array.shape[1])) - sensor_to_map.transform.translation.z
        # sparse map
        input_grid = np.reshape(v, robot_grid[0].shape)

        # TODO create model to get dense map from sparse
        # output = self.model.eval(input_grid.to(self.device))
        # dummy filling
        output = input_grid
        output[np.isnan(input_grid)] = -20

        # fill 2d map from output
        output_map2d = OccupancyMap(self.map_frame, self.grid_res)
        output_map2d.voxel_map.set_voxels(robot_grid_mapframe_2d, np.zeros_like(output.ravel(), dtype=np.float64), output.ravel())
        if DEBUG:
            self.output_2d_pc_pub.publish(output_map2d.to_pc_msg(self.map_frame, -np.inf))

        # convert output to 3d map
        output_map3d = OccupancyMap(self.map_frame, self.grid_res)
        output_map3d = output_map2d.to_3d(output_map3d, sensor_to_map)
        if DEBUG:
            self.output_3d_pc_pub.publish(output_map3d.to_pc_msg(self.map_frame))



if __name__ == '__main__':
    s2d = DEM_sparse2dense()
    rospy.spin()
