import sys
import os
import os.path


def make_dataset(dir_path, dataset_path):
    if os.path.exists(dir_path):
        my_dir_path = dir_path
    else:
        my_dir_path = dir_path[:7] + dir_path[14:]
    filelist = [os.path.join(dir_path, f) for f in os.listdir(my_dir_path)]

    cnt = 0
    for bagpath in filelist:
        if bagpath.endswith(".bag"):
            bagname = os.path.basename(bagpath)
            if os.path.exists(bagpath):
                bagpath = bagpath
                my_path = dataset_path
            else:
                bagpath = bagpath[:7] + bagpath[14:]
                my_path = dataset_path[:7] + dataset_path[14:]
                if not os.path.exists(bagpath) or not os.path.exists(my_path):
                    continue
            # MAKE DIRECTORY WHERE TO SAVE DATA
            command = "mkdir " + my_path + str(cnt)
            os.system(command)

            # MAKE TRAJECTORY
            command = "roslaunch /home/nvidia/workspace/src/traversability_estimation/launch/replay.launch bag:=" + bagpath + " directory:=" + my_path + str(cnt) + "/" + " mode:=trajectory bagname:=" + bagname + " rate:=0.8"
            os.system(command)

            # MAKE DATA AND LABELS
            command = "roslaunch /home/nvidia/workspace/src/traversability_estimation/launch/replay.launch bag:=" + bagpath + " directory:=" + my_path + str(cnt) + "/" + " mode:=dataset bagname:=" + bagname + " rate:=0.1"
            os.system(command)
            cnt += 1
    return


if __name__ == "__main__":
    make_dataset(sys.argv[1], sys.argv[2])

