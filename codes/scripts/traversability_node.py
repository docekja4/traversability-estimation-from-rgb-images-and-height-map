#!/usr/bin/env python
import rospy
import roslib
import message_filters
import sys
import os
from sensor_msgs.msg import PointCloud2, CompressedImage, CameraInfo
from image_geometry import PinholeCameraModel
import ros_numpy
import tf2_ros
import tf.transformations
import numpy as np
import cv2
import matplotlib.pyplot as plt
import math
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as tdata
from node_utils import *
from OccupancyMap import OccupancyMap, filter_pc
from ERFNet import load_model
import DEM_sparse2dense
import queue
from network_s2d import Net
from net_traversability import TraversabilityNet

counter = 0
DEBUG = True

class CameraNode:
    def __init__(self, directory, bagname, mode):
        self.directory = directory
        self.bagname = bagname
        self.mode = mode
        self.counter = 0

        self.map_frame = 'map'
        self.tf = tf2_ros.Buffer(cache_time=rospy.Duration(20))
        self.tf_sub = tf2_ros.TransformListener(self.tf)

        if mode == "trajectory":
            self.subscriber = rospy.Subscriber("scan_point_cloud", PointCloud2, self.callback_trajectory)
            self.x_arr = []
            self.y_arr = []
            self.z_arr = []
            if DEBUG:
                self.map = OccupancyMap(self.map_frame, 0.1)
                self.publisher = rospy.Publisher('trajectory_topic', PointCloud2, queue_size=2)
        else:
            self.grid_resolution = 0.1
            self.erfnet_model = load_model()
            if torch.cuda.is_available():
                self.device = torch.device('cuda')
            else:
                self.device = torch.device('cpu')

            self.trajectory_map = OccupancyMap(self.map_frame, resolution=self.grid_resolution, free_update=1.0)
            if mode == "dataset":
                x = np.load(self.directory + self.bagname + "_x.npy")
                y = np.load(self.directory + self.bagname + "_y.npy")
                origins = np.vstack((x, y, np.ones_like(x) * self.grid_resolution / 2))
                directions = np.array([np.cos(np.arange(-3.14, 3.14, 0.03)), np.sin(np.arange(-3.14, 3.14, 0.03)),
                                       np.zeros_like(np.arange(-3.14, 3.14, 0.03))]) * 0.3
                for i in range(len(x)):
                    traj_point = np.ones_like(directions[0]) * origins[:, i][np.newaxis].T
                    self.trajectory_map.voxel_map.update_lines(traj_point, directions + traj_point)

            cam_info_topic = rospy.get_param("~camera_info_topic", "/viz/camera_0/camera_info")
            cam_info_topic2 = rospy.get_param("~camera_info_topic2", "/viz/camera_1/camera_info")
            cam_info_topic3 = rospy.get_param("~camera_info_topic3", "/viz/camera_2/camera_info")
            cam_info_topic4 = rospy.get_param("~camera_info_topic4", "/viz/camera_3/camera_info")
            cam_info_topic5 = rospy.get_param("~camera_info_topic5", "/viz/camera_4/camera_info")
            self.camera_ids = [cam_info_topic, cam_info_topic2, cam_info_topic3, cam_info_topic4, cam_info_topic5]

            print('waiting for camera info')
            self.camera_model = {}
            for i in range(len(self.camera_ids)):
                msg = rospy.wait_for_message(self.camera_ids[i], CameraInfo)
                self.camera_model[msg.header.frame_id] = PinholeCameraModel()
                self.camera_model[msg.header.frame_id].fromCameraInfo(msg)
            print('camera info recieved')

            self.tracing_map = OccupancyMap(self.map_frame, self.grid_resolution)

            pcd_sub = message_filters.Subscriber(rospy.get_param("~bigbox_topic", '/dynamic_point_cloud_bigbox'), PointCloud2)
            image0_sub = message_filters.Subscriber("/viz/camera_0/image/compressed", CompressedImage)
            image1_sub = message_filters.Subscriber("/viz/camera_1/image/compressed", CompressedImage)
            image2_sub = message_filters.Subscriber("/viz/camera_2/image/compressed", CompressedImage)
            image3_sub = message_filters.Subscriber("/viz/camera_3/image/compressed", CompressedImage)
            image4_sub = message_filters.Subscriber("/viz/camera_4/image/compressed", CompressedImage)

            time_synchro = message_filters.ApproximateTimeSynchronizer([pcd_sub, image0_sub, image1_sub, image2_sub, image3_sub, image4_sub], 10, 1)
            time_synchro.registerCallback(self.callback)

            self.input_3d_map = OccupancyMap(self.map_frame, self.grid_resolution)
            self.input_2d_map = OccupancyMap(self.map_frame, self.grid_resolution)
            self.visible_2d_map = OccupancyMap(self.map_frame, resolution=self.grid_resolution, free_update=1.0)
            self.visible_2d_map.voxel_map.hit_update = 1.0
            if DEBUG:
                self.map_3d_publisher = rospy.Publisher('output_3d_pc',  PointCloud2, queue_size=2)
            self.model_s2d = Net()
            self.model_s2d = self.model_s2d.to(torch.device("cpu"))
            self.model_s2d.load_state_dict(torch.load("/home/honza/bc_project/best_weights", map_location=torch.device("cpu")))
            # self.queue_origins = queue.Queue(5)
            # self.queue_pc_cloud = queue.Queue(5)

            self.model_traversability = TraversabilityNet()
            self.model_traversability.load_state_dict(torch.load("/home/honza/net_epoch_0264", map_location=self.device))

            self.publisher = rospy.Publisher('trajectory_topic', PointCloud2, queue_size=2)

    def callback(self, pointcloud, cam_0, cam_1, cam_2, cam_3, cam_4):
        global counter
        ## POINTCLOUD HANDLE
        start1 = time.time()
        pcd_to_map = self.tf.lookup_transform(self.map_frame, pointcloud.header.frame_id, pointcloud.header.stamp)
        pc_cloud = ros_numpy.numpify(pointcloud)
        pc_array_homogenous = np.concatenate([np.asarray([pc_cloud['x'].ravel(),pc_cloud['y'].ravel(),pc_cloud['z'].ravel()]), np.ones_like(pc_cloud['z'])[np.newaxis]])

        T = ros_numpy.numpify(pcd_to_map.transform)
        pc_cloud_map_frame = np.matmul(T, pc_array_homogenous)

        # create sensor origin in map frame
        origins = np.ones_like(pc_cloud['x']) * np.array(
            [pcd_to_map.transform.translation.x, pcd_to_map.transform.translation.y,
             pcd_to_map.transform.translation.z], )[np.newaxis].T

        # create 3D voxel map
        # FIFO approach
        self.input_3d_map.voxel_map.clear()

        # without FIFO
        self.input_3d_map.voxel_map.update_lines(origins, pc_cloud_map_frame[:3, :])
        self.input_3d_map = filter_pc(self.input_3d_map)
        # start1 = time.time()
        self.input_2d_map = self.input_3d_map.to_2d(self.input_2d_map, pcd_to_map)
        # print("To 2D took: " + str(time.time() - start1))

        # grid in robot frame
        robot_grid = np.meshgrid(np.arange(-12.75, 12.85, self.grid_resolution), np.arange(-12.75, 12.85, self.grid_resolution))
        robot_grid_array = np.asarray([robot_grid[0].ravel(), robot_grid[1].ravel(), np.ones_like(robot_grid[1].ravel()) * self.grid_resolution])
        # to homogeneous coordinates
        robot_grid_array = np.concatenate([robot_grid_array, np.ones(robot_grid_array.shape[1])[np.newaxis]])  # not sure about z=0.1
        # transform robot grid to map frame
        robot_grid_mapframe = np.matmul(T, robot_grid_array)
        # fill the input from 2d map
        robot_grid_mapframe_2d = np.concatenate([robot_grid_mapframe[0:2, :], np.ones(robot_grid_array.shape[1])[np.newaxis] * self.grid_resolution / 2])  # x,y,z in 2.5d map
        v = self.input_2d_map.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(robot_grid_array.shape[1])) - pcd_to_map.transform.translation.z
        # sparse map
        input_grid = np.reshape(v, robot_grid[0].shape)
        mask = torch.from_numpy((~np.isnan(input_grid)).astype(np.float32)).to(torch.device("cpu")).unsqueeze(0).unsqueeze(0)
        input_grid[np.isnan(input_grid)] = 0
        input = torch.from_numpy(input_grid.astype(np.float32)).to(torch.device("cpu")).unsqueeze(0).unsqueeze(0)

        # dummy fill
        output = self.model_s2d(input, mask)
        output = output.detach().numpy()

        # fill 2d map from output
        output_map2d = OccupancyMap(self.map_frame, self.grid_resolution)
        output_map2d.voxel_map.set_voxels(robot_grid_mapframe_2d, np.zeros_like(output.ravel(), dtype=np.float64), output.ravel())
        # convert output to 3d map
        self.tracing_map = OccupancyMap(self.map_frame, self.grid_resolution)
        self.tracing_map = output_map2d.to_3d_dem(self.tracing_map, pcd_to_map)
        if DEBUG:
            self.map_3d_publisher.publish(self.tracing_map.to_pc_msg(self.map_frame))
        print("3D dem evaluated in: " + str(time.time() - start1))

        # get transformations
        sensor_to_map_0 = self.tf.lookup_transform(self.map_frame, cam_0.header.frame_id, cam_0.header.stamp)
        sensor_to_map_1 = self.tf.lookup_transform(self.map_frame, cam_1.header.frame_id, cam_1.header.stamp)
        sensor_to_map_2 = self.tf.lookup_transform(self.map_frame, cam_2.header.frame_id, cam_2.header.stamp)
        sensor_to_map_3 = self.tf.lookup_transform(self.map_frame, cam_3.header.frame_id, cam_3.header.stamp)
        sensor_to_map_4 = self.tf.lookup_transform(self.map_frame, cam_4.header.frame_id, cam_4.header.stamp)

        dim_reduction = 2
        # image indices
        pix_w = np.linspace(0, 1232 / (2 * dim_reduction), num=60, dtype=np.int, endpoint=False)
        # pix_h = np.linspace(0, 1616 / (2 * dim_reduction), num=80, dtype=np.int, endpoint=False)
	pix_h = np.linspace(0, 1616 / (2 * dim_reduction) - 1616 / (4 * 2 * dim_reduction), num=60, dtype=np.int, endpoint=False)

        ## IMAGES eval
        features_0 = self.get_features(cam_0, dim_reduction=dim_reduction)
        features_1 = self.get_features(cam_1, dim_reduction=dim_reduction)
        features_2 = self.get_features(cam_2, dim_reduction=dim_reduction)
        features_3 = self.get_features(cam_3, dim_reduction=dim_reduction)
        features_4 = self.get_features(cam_4, dim_reduction=dim_reduction)

        h_0, pick_features_0 = self.trace_features(sensor_to_map_0, cam_0, pix_w, pix_h, features_0, dim_reduction=dim_reduction, max_range=20.0, min_val=-100.0, max_val=0.0)
        h_1, pick_features_1 = self.trace_features(sensor_to_map_1, cam_1, pix_w, pix_h, features_1, dim_reduction=dim_reduction, max_range=20.0, min_val=-100.0, max_val=0.0)
        h_2, pick_features_2 = self.trace_features(sensor_to_map_2, cam_2, pix_w, pix_h, features_2, dim_reduction=dim_reduction, max_range=20.0, min_val=-100.0, max_val=0.0)
        h_3, pick_features_3 = self.trace_features(sensor_to_map_3, cam_3, pix_w, pix_h, features_3, dim_reduction=dim_reduction, max_range=20.0, min_val=-100.0, max_val=0.0)
        h_4, pick_features_4 = self.trace_features(sensor_to_map_4, cam_4, pix_w, pix_h, features_4, dim_reduction=dim_reduction, max_range=20.0, min_val=-100.0, max_val=0.0)

        h = np.hstack((h_0, h_1, h_2, h_3, h_4))
        pick_features = np.vstack((pick_features_0, pick_features_1, pick_features_2, pick_features_3, pick_features_4))

        # prepare feature map grids
        feature_map = OccupancyMap(self.map_frame, self.grid_resolution)
        h_xy = np.concatenate([h[0:2, :], np.ones(h.shape[1])[np.newaxis] * self.grid_resolution / 2])
        try:
            check = pick_features[0]
        except:
            return

        # set features to voxel map
        for i in range(len(pick_features[0]) + 1):
            if i == 0:
                # feature_map.voxel_map.set_voxels(h_xy, np.ones((h.shape[1], 1)) * i, h[2:3])
                feature_map.voxel_map.set_voxels(robot_grid_mapframe_2d, np.zeros_like(output.ravel(), dtype=np.float64),
                                      output.ravel())
            else:
                feature_map.voxel_map.set_voxels(h_xy, np.ones((h.shape[1], 1)) * i, pick_features[:, i-1])
        # make grid with corresponding features
        for i in range(len(pick_features[0]) + 1):
            v = feature_map.voxel_map.get_voxels(robot_grid_mapframe_2d, np.ones(robot_grid_array.shape[1]) * i)
            if i == 0:
                features = np.reshape(v, (1, robot_grid[0].shape[0], robot_grid[0].shape[1]))
            else:
                grid = np.reshape(v, (1, robot_grid[0].shape[0], robot_grid[0].shape[1]))
                features = np.concatenate((features, grid))
        directions = np.array([np.cos(np.arange(-3.14, 3.14, 0.03)), np.sin(np.arange(-3.14, 3.14, 0.03)), np.zeros_like(np.arange(-3.14, 3.14, 0.03))]) * 20
        min_val = -0.4 + pcd_to_map.transform.translation.z
        max_val = 0.1 + pcd_to_map.transform.translation.z
        max_range = 15.0
        trace_origin = origins[:, 0][np.newaxis].T
        trace_origin[2] = self.grid_resolution / 2

        [h, v] = self.input_2d_map.voxel_map.trace_rays(np.ones_like(directions) * trace_origin, directions, max_range, min_val, max_val, False)
        self.visible_2d_map.voxel_map.update_lines(np.ones_like(directions) * trace_origin, h)

        if self.mode == "dataset":
            np.save(self.directory + self.bagname + "_features_" + str(self.counter) + ".npy", features)
            plt.imsave(self.directory + self.bagname + "_features_" + str(self.counter) + ".png", features[0])
            labels, visible_mask = self.estimate_labels(features[0], robot_grid_mapframe_2d, grid_resolution=self.grid_resolution)
            np.save(self.directory + self.bagname + "_labels_" + str(self.counter) + ".npy", labels)
            plt.imsave(self.directory + self.bagname + "_labels_" + str(self.counter) + ".png", labels)
            np.save(self.directory + self.bagname + "_visible_mask_" + str(self.counter) + ".npy", visible_mask)
            plt.imsave(self.directory + self.bagname + "_visible_mask_" + str(self.counter) + ".png", visible_mask)
            # vis_im = features[0].copy()
            # vis_im[vis_im == -20] = 0
            # plt.imsave("/home/honza/bc_project/try/" + self.bagname + "_heightmap.png", vis_im)
            # np.save("/home/honza/bc_project/try/heightmap.npy", vis_im)
            self.counter += 1
        else:
            visible_mask = self.visible_2d_map.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(robot_grid_mapframe_2d.shape[1]))
            visible_mask = np.reshape(visible_mask, features[0].shape)
            visible_mask = ~np.isnan(visible_mask).reshape((256, 256))
            visible_mask = visible_mask.astype(np.float32)
            visible_mask = torch.from_numpy(visible_mask).to(self.device).unsqueeze(0).unsqueeze(0)
            features[np.isnan(features)] = 0
            features = torch.from_numpy(features.astype(np.float32)).to(self.device).unsqueeze(0)
            with torch.no_grad():
                output_traversability = self.model_traversability(features, visible_mask)
                output_traversability = output_traversability.detach().numpy()
                output_traversability = np.argmax(output_traversability, 1).astype(np.float32)

            output_map2d_traversability = OccupancyMap(self.map_frame, self.grid_resolution)
            output_map2d_traversability.voxel_map.set_voxels(robot_grid_mapframe_2d, np.zeros_like(output.ravel(), dtype=np.float64),
                                              output_traversability.ravel())
            if DEBUG:
                self.publisher.publish(output_map2d_traversability.to_3d_dem(output_map2d_traversability, pcd_to_map).to_pc_msg(self.map_frame, occ_value=-1))

            #TODO: nn to estimate traversability from feature maps
        if DEBUG:
            self.map_3d_publisher.publish(feature_map.to_pc_msg(self.map_frame))

        print("XD EZ, rays traced :)")
        print("Callback took: " + str(time.time() - start1))

    def callback_trajectory(self, data):
        # vytvoreni bodu na trajektorii, ulozeni jako npy array, v dalsim spusteni jsem schopnej to
        # hodit do  callbacku na elevation/map a mit to v mape upraveny
        self.counter += 1
        if self.counter == 10:
            try:
                transform = self.tf.lookup_transform(self.map_frame, 'base_link', data.header.stamp)
                self.x_arr.append(transform.transform.translation.x)
                self.y_arr.append(transform.transform.translation.y)
                self.z_arr.append(transform.transform.translation.z)

                np.save(self.directory + self.bagname + "_x.npy", np.array(self.x_arr))
                np.save(self.directory + self.bagname + "_y.npy", np.array(self.y_arr))
                np.save(self.directory + self.bagname + "_z.npy", np.array(self.z_arr))
                self.counter = 0
                print("Saving trajectory points. Number of points: " + str(len(self.x_arr)) + "\r")
                if DEBUG:
                    self.map.voxel_map.set_voxels(np.vstack((self.x_arr, self.y_arr, self.z_arr)), np.zeros_like(self.x_arr), np.ones_like(self.x_arr))
                    self.publisher.publish(self.map.to_pc_msg(self.map_frame))
            except:
                self.counter = 0

    def get_features(self, cam, dim_reduction=2):
        np_arr = np.fromstring(cam.data, np.uint8)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        dim = (int(image_np.shape[1] / dim_reduction), int(image_np.shape[0] / dim_reduction))
        image_np = cv2.resize(image_np, dim)
        image_np = image_np.reshape(1, image_np.shape[0], image_np.shape[1], image_np.shape[2]).astype('f4').transpose(
            0, 3, 1, 2)
        image_tensor = torch.from_numpy(image_np)
        image_tensor = image_tensor.to(self.device)

        features = self.erfnet_model(image_tensor)
        features = features.detach().cpu().numpy().transpose(0, 2, 3, 1)
        return features.reshape(features.shape[1], features.shape[2], features.shape[3])

    def trace_features(self, sensor_to_map, cam, pix_w, pix_h, features, dim_reduction=2, max_range=20.0, min_val=-100.0, max_val=0.0):
        pix_3d = []
        pick_features = []

        for y_ind in range(0, len(pix_h)):
            for x_ind in range(0, len(pix_w)):
                (u, v) = self.camera_model[cam.header.frame_id].rectifyPoint(
                    (pix_w[x_ind] * 2 * dim_reduction, pix_h[y_ind] * 2 * dim_reduction))
                pix_3d.append(self.camera_model[cam.header.frame_id].projectPixelTo3dRay((u, v)))
                # pick_features.append(features[1616 / (2 * dim_reduction) - y_ind - 1][x_ind])
		pick_features.append(features[pix_h[y_ind]][pix_w[x_ind]])

        pick_features = np.asarray(pick_features)

        directions = np.array(pix_3d).T
        directions_matrix = make_matrix(4, directions[0].size, [directions[0, :], directions[1, :], directions[2, :],
                                                                np.ones_like(directions[0, :])])
        origin = col(array(sensor_to_map.transform.translation))
        R = tf.transformations.quaternion_matrix(
            [sensor_to_map.transform.rotation.x, sensor_to_map.transform.rotation.y,
             sensor_to_map.transform.rotation.z,
             sensor_to_map.transform.rotation.w])
        directions_matrix = R * directions_matrix

        directions = np.asarray(directions_matrix[0:3])

        [h, v] = self.tracing_map.voxel_map.trace_rays(np.ones_like(directions) * origin, directions, max_range,
                                                       min_val, max_val, False)
        h = h[:, ~np.isnan(v)]
        pick_features = pick_features[~np.isnan(v)]

        return h, pick_features

    def estimate_labels(self, height_map, grid, grid_resolution=0.1):
        v = self.trajectory_map.voxel_map.get_voxels(grid, np.zeros(grid.shape[1]))
        trajectory = np.reshape(v, height_map.shape)
        labels = np.zeros_like(trajectory)
        x = np.array([grid_resolution, grid_resolution * 2, grid_resolution * 3]).reshape((1, 3))
        x = np.hstack((x, x, x)).T
        x = np.append(x, [0.2, 0.4, 0.2, 0.0]).reshape(13, 1)
        y = np.hstack((grid_resolution * np.ones((1, 3)), 2 * grid_resolution * np.ones((1, 3)),
                       3 * grid_resolution * np.ones((1, 3)))).T
        y = np.append(y, [0.4, 0.2, 0.0, 0.2]).reshape(13, 1)

        for i_x in range(2, height_map.shape[1] - 2):
            for i_y in range(2, height_map.shape[0] - 2):
                z = np.array([height_map[i_y + 1, i_x - 1], height_map[i_y + 1, i_x], height_map[i_y + 1, i_x + 1],
                              height_map[i_y, i_x - 1], height_map[i_y, i_x], height_map[i_y, i_x + 1],
                              height_map[i_y - 1, i_x - 1], height_map[i_y - 1, i_x], height_map[i_y - 1, i_x + 1],
                              height_map[i_y - 2, i_x], height_map[i_y, i_x + 2], height_map[i_y + 2, i_x],
                              height_map[i_y, i_x - 2]]).reshape(13, 1)
                # fit plane to points
                X = np.concatenate((x, y, np.ones((x.shape[0], 1))), axis=1)
                w = np.dot(np.linalg.pinv(X), z)

                # calculate the angle from horizontal plane
                alpha = np.arccos(1 / math.sqrt(w[0] ** 2 + w[1] ** 2 + 1))

                if abs(alpha) < 0.5:
                    # save info about stairs point
                    labels[i_y, i_x] = 1
                if abs(alpha) > 0.8:
                    labels[i_y, i_x] = 2
        labels[~np.isnan(trajectory)] = 1
        check_labels = labels.copy()
        for i_x in range(1, height_map.shape[1] - 1):
            for i_y in range(1, height_map.shape[0] - 1):
                curr_labels = np.array(
                    [labels[i_y + 1, i_x - 1], labels[i_y + 1, i_x], labels[i_y + 1, i_x + 1], labels[i_y, i_x - 1],
                     labels[i_y, i_x], labels[i_y, i_x + 1], labels[i_y - 1, i_x - 1], labels[i_y - 1, i_x],
                     labels[i_y - 1, i_x + 1]]).reshape(9, 1)
                traversable = np.sum(curr_labels == 1)
                non_traversable = np.sum(curr_labels == 2)
                if traversable >= 5:
                    check_labels[i_y, i_x] = 1
                elif non_traversable >= 5:
                    check_labels[i_y, i_x] = 2
        check_labels[~np.isnan(trajectory)] = 1
        v = self.visible_2d_map.voxel_map.get_voxels(grid, np.zeros(grid.shape[1]))
        visible_mask = np.reshape(v, height_map.shape)
        return check_labels, visible_mask


def start_node(args):
    rospy.init_node("traversability_node", anonymous=True)
    # camera_node = CameraNode_try("/viz/camera_0/image/compressed", "nic", "bag")

    ## CAMERA INDICES
    # camera_0 => front right
    # camera_1 => right
    # camera_2 => rear
    # camera_3 => left
    # camera_4 => front left

    directory = args[1]
    bagname = os.path.basename(args[2])
    mode = args[3]

    camera_node = CameraNode(directory, bagname, mode)

    rospy.spin()


if __name__ == '__main__':
    args = rospy.myargv(argv=sys.argv)
    start_node(args)

    #   notes: v test_codes je prototyp na labelovani. v podstate to na prvni pohled vypada celkem rozumne.
    #           staci do toho jen zakomponovat tu trajektorii robota. coz bude ez as foook
    #       idea na traversabilitu - z tech bodu taky muzu udelat voxel_mapu, a tu pak vzdy jen vycist a pouzit na labely
