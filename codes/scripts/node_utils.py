import os
import numpy as np
import struct
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as tdata

def float_to_bin(num):
    return format(struct.unpack('!I', struct.pack('!f', num))[0], '032b')


class Dataset(tdata.Dataset):
    def __init__(self, data_file):
        super(Dataset, self).__init__()
        self.data = data_file  # mmap is way faster for these large data

    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, i):
        return {
            'data': np.asarray(self.data[i]).astype('f4').transpose((2, 0, 1)) / 255,
            'key': i,  # for saving of the data
            # due to mmap, it is necessary to wrap your data in np.asarray. It does not add almost any overhead as it does not copy anything
        }


class Model(torch.nn.Module):
    '''Model for 2.5D segmentation of terrain traversability'''

    def __init__(self):
        super(Model, self).__init__()
        self.conv1 = nn.Conv2d(1, 16, 3)
        self.deconv1 = nn.ConvTranspose2d(16, 3, 3)

    def forward(self, x):
        # encoding path
        x = F.relu(self.conv1(x))
        x = self.deconv1(x)
        return x

    def weights_initialization(self):
        for layer in self.named_parameters():
            if type(layer) in {nn.Conv2d, nn.ConvTranspose2d}:
                nn.init.xavier_uniform_(layer.weight.data, gain=torch.sqrt(2))


class Model2(torch.nn.Module):
    '''Model for 2.5D segmentation of terrain traversability'''

    def __init__(self):
        super(Model2, self).__init__()
        self.conv1 = nn.Conv2d(1, 16, 3)
        self.conv2 = nn.Conv2d(16, 32, 3)
        self.deconv2 = nn.ConvTranspose2d(32, 16, 3)
        self.deconv1 = nn.ConvTranspose2d(16, 3, 3)

        self.batchnorm = nn.BatchNorm2d(16)
        self.batchnorm2 = nn.BatchNorm2d(32)
    def forward(self, x):
        # encoding path
        x = F.relu(self.conv1(x))
        x = self.batchnorm(x)
        x = F.relu(self.conv2(x))
        x = self.batchnorm2(x)

        # decoding path
        x = F.relu(self.deconv2(x))
        x = self.deconv1(x)
        return x

    def weights_initialization(self):
        for layer in self.named_parameters():
            if type(layer) in {nn.Conv2d, nn.ConvTranspose2d}:
                nn.init.xavier_uniform_(layer.weight.data, gain=torch.sqrt(2))


def normalization(array, nan_def):
    new_arr = array[array != nan_def]
    arr_min = min(new_arr)
    array[array != nan_def] = array[array != nan_def] - arr_min
    array[array == nan_def] = 0

    return array


def load_model():
    if torch.cuda.is_available():
        dev = torch.device('cuda')
    else:
        dev = torch.device('cpu')

    path = os.path.dirname(os.path.realpath(__file__)) + "/model_state_stairs.pt"
    model = Model()
    model.load_state_dict(torch.load(path, map_location=dev))
    model.eval()

    return model

def make_matrix(rows, cols, L):
    mat = np.matrix(L, dtype='float64')
    mat.resize((rows,cols))
    return mat

def array(msg):
    """Return message attributes (slots) as array."""
    return np.array(slots(msg))


def col(arr):
    """Convert array to column vector."""
    return arr.reshape((arr.size, 1))

def slots(msg):
    """Return message attributes (slots) as list."""
    return [getattr(msg, var) for var in msg.__slots__]
